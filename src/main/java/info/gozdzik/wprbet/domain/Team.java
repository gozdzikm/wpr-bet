package info.gozdzik.wprbet.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "teams")
public class Team implements Serializable
{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_teams")
	@SequenceGenerator(name = "seq_teams", sequenceName = "seq_teams", allocationSize = 10, initialValue = 10)
	@Column(insertable = false)
	private Long id;
	
	@Column(nullable = false, length = 64)
	private String name;
	
	@Enumerated(EnumType.STRING)
	private TeamType teamType;
	
	public Team()
	{
	}
	
	public Long getId()
	{
		return id;
	}
	
	@SuppressWarnings("unused")
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public TeamType getTeamType()
	{
		return teamType;
	}
	
	public void setTeamType(TeamType teamType)
	{
		this.teamType = teamType;
	}
}



