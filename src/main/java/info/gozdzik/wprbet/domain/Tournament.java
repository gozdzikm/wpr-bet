package info.gozdzik.wprbet.domain;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tournaments")
public class Tournament
{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tournaments")
	@SequenceGenerator(name = "seq_tournaments", sequenceName = "seq_tournaments", allocationSize = 10, initialValue = 10)
	@Column(insertable = false)
	private Long id;
	
	@Column(nullable = false, length = 64)
	private String name;
	
	public Tournament()
	{
	}
	
	public Long getId()
	{
		return id;
	}
	
	@SuppressWarnings("unused")
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
}
