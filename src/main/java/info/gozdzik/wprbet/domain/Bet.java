package info.gozdzik.wprbet.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "bets")
public class Bet implements Serializable
{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_bets")
	@SequenceGenerator(name = "seq_bets", sequenceName = "seq_bets", allocationSize = 10, initialValue = 10)
	@Column(insertable = false)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "match_id")
	private Match match;
	
	
	@Column(nullable = false)
	private LocalDateTime createTime;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "bet_event_id")
	private BetEvent betEvent;
	
	public Bet()
	{
	}
	
	public Long getId()
	{
		return id;
	}
	
	
	@SuppressWarnings("unused")
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public Match getMatch()
	{
		return match;
	}
	
	public void setMatch(Match match)
	{
		this.match = match;
	}
	
	public LocalDateTime getCreateTime()
	{
		return createTime;
	}
	
	public void setCreateTime(LocalDateTime createTime)
	{
		this.createTime = createTime;
	}
	
	public BetEvent getBetEvent()
	{
		return betEvent;
	}
	
	public void setBetEvent(BetEvent betEvent)
	{
		this.betEvent = betEvent;
	}
}
