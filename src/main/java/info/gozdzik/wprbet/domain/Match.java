package info.gozdzik.wprbet.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "matches")
public class Match implements Serializable
{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_matches")
	@SequenceGenerator(name = "seq_matches", sequenceName = "seq_matches", allocationSize = 10, initialValue = 10)
	@Column(insertable = false)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "host_id")
	private Team host;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "visitor_id")
	private Team visitor;
	
	@Column(nullable = false)
	private LocalDateTime createTime;
	
	@Column(nullable = false)
	private LocalDateTime startTime;
	
	@Column
	private Short hostScore;
	
	@Column
	private Short visitorScore;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "tournament_id")
	private Tournament tournament;
	
	public Match()
	{
	}
	
	public Long getId()
	{
		return id;
	}
	
	@SuppressWarnings("unused")
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public Team getHost()
	{
		return host;
	}
	
	public void setHost(Team host)
	{
		this.host = host;
	}
	
	public Team getVisitor()
	{
		return visitor;
	}
	
	public void setVisitor(Team visitor)
	{
		this.visitor = visitor;
	}
	
	public LocalDateTime getStartTime()
	{
		return startTime;
	}
	
	public void setStartTime(LocalDateTime startTime)
	{
		this.startTime = startTime;
	}
	
	public Short getHostScore()
	{
		return hostScore;
	}
	
	public void setHostScore(Short hostScore)
	{
		this.hostScore = hostScore;
	}
	
	public Short getVisitorScore()
	{
		return visitorScore;
	}
	
	public void setVisitorScore(Short visitorScore)
	{
		this.visitorScore = visitorScore;
	}
	
	public LocalDateTime getCreateTime()
	{
		return createTime;
	}
	
	public void setCreateTime(LocalDateTime createTime)
	{
		this.createTime = createTime;
	}
}
