package info.gozdzik.wprbet.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="susers")
public class User implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_susers")
	@SequenceGenerator(name = "seq_susers", sequenceName = "seq_susers", allocationSize = 10, initialValue = 10)
	@Column(insertable = false)
	private Long id;
	
	@Column(name = "login", nullable = false, unique = true, length = 128)
	private String login;
	
	public User()
	{
	}
	
	public Long getId()
	{
		return id;
	}
	
	@SuppressWarnings("unused")
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public String getLogin()
	{
		return login;
	}
	
	public void setLogin(String login)
	{
		this.login = login;
	}
}
