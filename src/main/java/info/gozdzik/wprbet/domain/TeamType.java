package info.gozdzik.wprbet.domain;

public enum TeamType
{
	
	NATIONAL("NATIONAL"),
	NATIONAL_U21("NATIONAL_U21"),
	CLUB ("CLUB");
	
	public static final TeamType[] ALL = {NATIONAL, NATIONAL_U21, CLUB};
	private final String name;
	
	private TeamType(final String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	@Override
	public String toString()
	{
		return getName();
	}
}
