package info.gozdzik.wprbet.domain;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "user_bets")
public class UserBet
{
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_user_bets")
	@SequenceGenerator(name = "seq_user_bets", sequenceName = "seq_user_bets", allocationSize = 10, initialValue = 10)
	@Column(insertable = false)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "bet_id")
	private Bet bet;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "user_id")
	private User user;
	
	
	@Column(nullable = false)
	private LocalDateTime betTime;
	
	@Column
	private Short hostScore;
	
	@Column
	private Short visitorScore;
	
	@Column(nullable = false, columnDefinition = "smallint DEFAULT 0")
	private Short score;
	
	public UserBet()
	{
	}
	
	public Long getId()
	{
		return id;
	}
	
	@SuppressWarnings("unused")
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public Bet getBet()
	{
		return bet;
	}
	
	public void setBet(Bet bet)
	{
		this.bet = bet;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public void setUser(User user)
	{
		this.user = user;
	}
	
	public LocalDateTime getBetTime()
	{
		return betTime;
	}
	
	public void setBetTime(LocalDateTime betTime)
	{
		this.betTime = betTime;
	}
	
	public Short getHostScore()
	{
		return hostScore;
	}
	
	public void setHostScore(Short hostScore)
	{
		this.hostScore = hostScore;
	}
	
	public Short getVisitorScore()
	{
		return visitorScore;
	}
	
	public void setVisitorScore(Short visitorScore)
	{
		this.visitorScore = visitorScore;
	}
	
	public Short getScore()
	{
		return score;
	}
	
	public void setScore(Short score)
	{
		this.score = score;
	}
}
