package info.gozdzik.wprbet.dao;

import info.gozdzik.wprbet.domain.Team;

public interface TeamDao extends Dao<Team>
{}
