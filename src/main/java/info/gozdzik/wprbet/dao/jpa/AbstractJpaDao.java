package info.gozdzik.wprbet.dao.jpa;

import static org.springframework.util.Assert.notNull;

import info.gozdzik.wprbet.dao.Dao;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;

public abstract class AbstractJpaDao<T extends Object> implements Dao<T>
{
	
	@PersistenceContext
	private EntityManager entityManager;
	private Class<T> domainClass;
	
	public EntityManager getEntityManager()
	{
		return entityManager;
	}
	
	@Override
	public void create(T t)
	{
		notNull(t, getDomainClassName() + " can't be null");
		getEntityManager().persist(t);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T get(Serializable id)
	{
		notNull(id, "id can't be null");
		return getEntityManager().find(getDomainClass(), id);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T load(Serializable id)
	{
		notNull(id, "id can't be null");
		
		T t = get(id);
		if (t == null)
		{
			throw new RuntimeException("No such " + getDomainClassName() + ": " + id);
		}
		return t;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<T> getAll()
	{
		return getEntityManager().createQuery("FROM " + getDomainClassName()).getResultList();
	}
	
	@Override
	public void update(T t)
	{
		notNull(t, getDomainClassName() + " can't be null");
		getEntityManager().merge(t);
	}
	
	@Override
	public void delete(T t)
	{
		notNull(t, getDomainClassName() + " can't be null");
		getEntityManager().remove(t);
	}
	
	@Override
	public void deleteById(Serializable id)
	{
		notNull(id, "id can't be null");
		getEntityManager().createQuery("DELETE FROM " + getDomainClassName() + " WHERE id = :id ")
			.setParameter("id", id).executeUpdate();
	}
	
	@Override
	public void deleteAll()
	{
		getEntityManager().createQuery("DELETE FROM " + getDomainClassName()).executeUpdate();
	}
	
	@Override
	public long count()
	{
		return (Long) getEntityManager().createQuery("SELECT COUNT(*) FROM " + getDomainClassName())
			.getSingleResult();
	}
	
	@Override
	public boolean exists(Serializable id)
	{
		notNull(id, "id can't be null");
		return (get(id) != null);
	}
	
	@SuppressWarnings("unchecked")
	private Class<T> getDomainClass()
	{
		if (domainClass == null)
		{
			ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
			this.domainClass = (Class<T>) thisType.getActualTypeArguments()[0];
		}
		return domainClass;
	}
	
	private String getDomainClassName()
	{
		return getDomainClass().getName();
	}
	
	private String getDomainTableName()
	{
		Table table = getDomainClass().getAnnotation(Table.class);
		return table.schema() + "." + table.name();
	}
	
}