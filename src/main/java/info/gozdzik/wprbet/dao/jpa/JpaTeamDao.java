package info.gozdzik.wprbet.dao.jpa;

import info.gozdzik.wprbet.dao.TeamDao;
import info.gozdzik.wprbet.domain.Team;
import org.springframework.stereotype.Repository;

@Repository
public class JpaTeamDao extends AbstractJpaDao<Team> implements TeamDao
{}
