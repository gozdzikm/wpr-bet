package info.gozdzik.wprbet.service;

import info.gozdzik.wprbet.dao.TeamDao;
import info.gozdzik.wprbet.domain.Team;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

@Service
@Transactional(readOnly = true)
public class TeamServiceImpl implements TeamService
{
	
	@Autowired
	private TeamDao teamDao;
	
	@Override
	public List<Team> getTeams()
	{
		return teamDao.getAll();
	}
	
	@Override
	public Team getTeam(Long id)
	{
		return teamDao.get(id);
	}
	
	@Transactional(readOnly = false)
	@Override
	public boolean addTeam(Team team, Errors errors)
	{
		validateName(team.getName(), errors);
		boolean valid = !errors.hasErrors();
		if (valid)
		{
			teamDao.create(team);
		}
		return valid;
	}
	
	@Override
	public boolean updateTeam(Team team, Errors errors)
	{
		validateName(team.getName(), errors);
		boolean valid = !errors.hasErrors();
		if (valid)
		{
			teamDao.update(team);
		}
		return valid;
	}
	
	@Override
	public void removeTeam(Long id)
	{
		teamDao.deleteById(id);
	}
	
	private void validateName(String name, Errors errors)
	{
		teamDao.getAll().stream().filter(t -> t.getName().equals(name)).findAny().ifPresent(
			i -> errors.rejectValue("name", "administration.teams.form.error.duplicate",
				new String[]{name}, null));
	}
}
