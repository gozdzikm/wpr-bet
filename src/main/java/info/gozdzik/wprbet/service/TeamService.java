package info.gozdzik.wprbet.service;

import info.gozdzik.wprbet.domain.Team;
import java.util.List;
import org.springframework.validation.Errors;

public interface TeamService
{
	List<Team> getTeams();
	
	Team getTeam(Long id);
	
	boolean addTeam(Team team, Errors errors);
	
	boolean updateTeam(Team team, Errors errors);
	
	void removeTeam(Long id);
}
