package info.gozdzik.wprbet.web;

import info.gozdzik.wprbet.domain.Team;
import info.gozdzik.wprbet.domain.TeamType;
import info.gozdzik.wprbet.form.TeamForm;
import info.gozdzik.wprbet.service.TeamService;
import java.util.Arrays;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/teams")
public class TeamController
{
	@Autowired
	private TeamService teamService;
	
	@ModelAttribute("teamTypes")
	public List<TeamType> populateTeamTypes() {
		return Arrays.asList(TeamType.ALL);
	}
	
	@ModelAttribute("teams")
	public List<Team> populateTeams()
	{
		return teamService.getTeams();
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String teams(Model model)
	{
		model.addAttribute("teamForm", new TeamForm());
		return "teams/index";
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String add(@Valid TeamForm teamForm, BindingResult result, Model model)
	{
		teamService.addTeam(toTeam(teamForm), result);
		return result.hasErrors() ? "teams/index" : "redirect:/teams/index";
	}
	
	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	public String remove(@RequestParam("teamId") Long teamId, Model model)
	{
		teamService.removeTeam(teamId);
		return "redirect:/teams/index";
	}
	
	//edycja druzyny
	
	private static Team toTeam(TeamForm teamForm)
	{
		Team team = new Team();
		team.setName(teamForm.getName());
		team.setTeamType(teamForm.getTeamType());
		return team;
	}
}
