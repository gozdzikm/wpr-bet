package info.gozdzik.wprbet.form;

import info.gozdzik.wprbet.domain.TeamType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

public class TeamForm
{
	@Null
	private Long id;
	
	@NotNull(message = "{administration.teams.form.name.notnull}")
	@Size(min = 3, max = 64, message = "{administration.teams.form.name.size}")
	private String name;
	
	@NotNull
	private TeamType teamType;
	
	public Long getId()
	{
		return id;
	}
	
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public TeamType getTeamType()
	{
		return teamType;
	}
	
	public void setTeamType(TeamType teamType)
	{
		this.teamType = teamType;
	}
}
